<?php namespace Bitcraft\Publish\Models;

use Backend\Facades\BackendAuth;
use Carbon\Carbon;
use Model;

/**
 * Model
 */
class QueueJob extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;
    protected $jsonable = ['payload'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'bitcraft_publish_queue_jobs';
    public $appends = ['available_date', 'created_date', 'type', 'user'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public function getAvailableDateAttribute(): string
    {
        return Carbon::createFromTimestampUTC($this->available_at)->format('d.m.Y H:i');
    }

    public function getCreatedDateAttribute(): string
    {
        return Carbon::createFromTimestampUTC($this->created_at)->format('d.m.Y H:i');
    }

    public function getTypeAttribute(): string
    {
        if (is_array($this->payload['data'])
            &&
            array_key_exists('id', $this->payload['data'])
            &&
            $page = Page::find($this->payload['data']['id'])) {
            return "Page with title: $page->title";
        }

        return 'ALL';
    }

    public function getUserAttribute() {
        return $this->payload['data']['user'];
    }

    public function beforeSave()
    {
        $this->user =  BackendAuth::getUser()->login;
    }
}
