<?php namespace Bitcraft\publish\Controllers;

use Backend\Classes\Controller;
use Backend\Facades\BackendAuth;
use BackendMenu;
use Bitcraft\Publish\Jobs\UploadPages;
use Carbon\Carbon;
use Illuminate\Support\Facades\Queue;

class Deployments extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\RelationController',];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Bitcraft.Publish', 'main-menu-item-publish', 'side-menu-item-deployment');
    }

    public function onPublishToS3()
    {
        $publish_at = post('publish_at');
        $date = empty($publish_at) ? Carbon::now() : Carbon::createFromTimeString($publish_at);
        Queue::later($date, UploadPages::class, ['user' => BackendAuth::getUser()->login]);
    }
}
