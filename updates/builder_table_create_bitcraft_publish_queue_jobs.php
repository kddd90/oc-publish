<?php namespace Bitcraft\Publish\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBitcraftPublishQueueJobs extends Migration
{
    public function up()
    {
        Schema::create('bitcraft_publish_queue_jobs', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('created_at')->nullable();
            $table->string('queue')->nullable();
            $table->text('payload')->nullable();
            $table->integer('reserved_at')->nullable();
            $table->integer('available_at')->nullable();
            $table->integer('attempts')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('bitcraft_publish_queue_jobs');
    }
}
