<?php namespace Bitcraft\Publish\Classes;

use Aws\CloudFront\CloudFrontClient;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

class Cloudfront
{

    public static function generateRandomString($length = 10): string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            try {
                $randomString .= $characters[random_int(0, $charactersLength - 1)];
            } catch (\Exception $e) {
                Log::error($e->getMessage());
            }
        }
        return $randomString;
    }

    public static function invalidate($distribution_id)
    {
        $caller = self::generateRandomString(16);

        $cloudFront = App::make('aws')->createClient('cloudfront');

        $cloudFront->createInvalidation([
            'DistributionId' => $distribution_id,
            'InvalidationBatch' => [
                'CallerReference' => $caller,
                'Paths' => [
                    'Items' => ['/*'],
                    'Quantity' => 1
                ]
            ]
        ]);
    }
}
